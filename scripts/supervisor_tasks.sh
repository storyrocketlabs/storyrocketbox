#!/usr/bin/env bash
rm /etc/supervisor/conf.d/*

cat <<EOT >> /etc/supervisor/conf.d/laravel-worker.conf
[program:laravel-worker]
process_name=%(program_name)s_%(process_num)02d
command=php /storyrocket/artisan queue:work redis --sleep=3 --tries=3
autostart=true
autorestart=true
numprocs=8
redirect_stderr=true
stdout_logfile=/storyrocket/worker.log
EOT

supervisorctl reread
supervisorctl update