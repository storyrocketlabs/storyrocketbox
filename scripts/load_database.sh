#!/bin/bash
user=homestead
password=secret
database=storyrocket

mysql --user="$user" --password="$password" --execute="DROP DATABASE IF EXISTS $database; CREATE DATABASE $database;"
mysql --user="$user" --password="$password" --database="$database" < /vagrant/sql/db_storyrocket.sql

database=storyrocket_api
mysql --user="$user" --password="$password" --execute="DROP DATABASE IF EXISTS $database; CREATE DATABASE $database;"