apt-get -y install gcc make autoconf libc-dev pkg-config
apt-get install -y php-redis
service nginx restart
service php7.2-fpm restart

#!/bin/bash
cd /storyrocket
sudo composer install

cd /storyrocketapi
sudo composer install