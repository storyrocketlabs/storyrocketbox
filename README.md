# Story Rocket Box #

Pasos necesarios para instalar y configurar el entorno de desarrollo.

### Este repositorio contiene ###

* La base de datos de storyrocket.
* Configuración y archivos de la imagen de vagrant a utilizar para el desarrollo.

## Primeros Pasos ##


### Descargar e instalar vagrant + virtualbox ###

1. Descargar la última versión de vagrant:

    https://www.vagrantup.com/downloads.html

2. Instalar y seleccionar para instalar tambien **VirtualBox** como proveedor.

3. Instalar el plugin vagrant-hostupdater (permite configurar dominios locales automáticamente)

        vagrant plugin install vagrant-hostsupdater
         
### Clonar el repositorio storyrocket  ###
- El repositorio debe ser clonado en la misma carpeta donde se encuentra la capeta del storyrocket-box, es decir al mismo nivel.

        git clone https://bitbucket.org/storyrocketlabs/storyrocket.git
    
### Clonar el repositorio storyrocketapi  ###
- El repositorio debe ser clonado en la misma carpeta donde se encuentra la capeta del storyrocket-box, es decir al mismo nivel.
    
        git clone https://bitbucket.org/storyrocketlabs/storyrocketapi.git

### Creación del archivo .env ###

- Crear el archivo **.env** utilizando como plantilla el archivo **.env.example** en ambos proyectos storyrocket y storyrocketapi.
- Ejecutar el comando:
        
        php artisan key:generate

### Inicializar la máquina virtual  ###

_En la carpeta del box:_ 

    vagrant up

Si es que corre sin problemas, ya se puede ingresar a la página en desarrollo

https://storyrocket.test

https://api.storyrocket.test

### Instalar modulos de node  ###
- Carpeta storyrocket, instalar los modulos de node necesario apra usar laravel mix.
    
      npm install o yarn install